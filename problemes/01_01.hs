 
{-|
P77907

Write a function absValue :: Int -> Int that, given an integer, returns its absolute value.

Write a function power :: Int -> Int -> Int that, given an integer x and a natural p, returns thep-th power of x, that is, xp.

Write a function isPrime :: Int -> Bool that, given a natural, tells whether it is a prime number or not.

Write a function slowFib :: Int -> Int that returns the n-th element of the Fibonacci sequence using the recursive algorithm that defines it (f(0)=0, f(1)=1, f(n)=f(n−1)+f(n−2) for n≥ 2).

Write a function quickFib :: Int -> Int that returns the n-th element of the Fibonacci sequence using a more efficient algorithm.    
-}

absValue :: Int -> Int
absValue n
    | n >= 0    = n
    | otherwise = -n
    
    
power :: Int -> Int -> Int 
power x p
    | p == 0     = 1
    | otherwise = x * (power x (p-1) ) --importants parèntesis!!!
    
isPrime :: Int -> Bool
isPrime n
    | n == 0    = False
    | n == 1    = False
    | otherwise = ( null [x |  x<- [2..n-1], n `mod` x == 0] )
    
slowFib :: Int -> Int
slowFib n
    | n == 0    = 0
    | n == 1    = 1
    | otherwise = slowFib(n-1) + slowFib(n-2)
  
  
fibaux :: Int -> Int -> Int -> Int
fibaux f0 f1 n
    | n == 0    = f0
    | n == 1    = f1
    | otherwise = fibaux (f1) (f0 + f1) (n-1)
    

quickFib = fibaux 0 1
    
    
    
    
