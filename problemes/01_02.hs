
{-
    Define a function myLength :: [Int] -> Int that, given a list of integers, returns its length.
    Define a function myMaximum :: [Int] -> Int that, given a non-empty list of integers, returns its maximal element.
    Define a function average :: [Int] -> Float that, given a non-empty list of integers, returns its average.
    Define a function buildPalindrome :: [Int] -> [Int] that, given a list, returns its palindrome that starts with the reserved list.
    Define a function remove :: [Int] -> [Int] -> [Int] that given a list of integers x and a list of integers y, returns x after having removed all the ocurrences of the elements in y.
    Define a function flatten :: [[Int]] -> [Int] that flattens a list of lists yielding a single list of elements.
    Define a function oddsNevens :: [Int] -> ([Int],[Int]) that, given a list of integers, returns two lists: Onw with all the even numbers and one with all the odd numbers, each of them in the same relative order as in the original list.
    Define a function primeDivisors :: Int -> [Int] that returns the list of prime divisors of a non-zero natural.
-}

myLength :: [Int] -> Int 
myLength []      = 0
myLength (x:xs)  = 1 + myLength xs 
    
    
    
    
